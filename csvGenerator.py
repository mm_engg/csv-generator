'''
import files
'''
import urllib
import urllib2
import shutil
import re
import subprocess 
import os
from xml.dom import minidom
import math

'''
Given an inout file with a no. of mpd files, generates a list appending each mpd file to it
@params: inputFile - that contains uls of a no of .mpd files
@returns: data -list of mpd url
'''
def generateMpdList(inputFile):
	count=0
	data=[]
	regex = re.compile("http://\S+\.mpd")
	file = open(inputFile, "r")
	for line in file:
		if line !='':
			result = regex.findall(line)
			length=len(result)
			count=count+length
			data.extend(result)
	#print (data)
	file.close
	print "No of occurances: ",count
	return data



'''
Function to count no of mpd urls in the mpd.txt and pass each url along with its index no to the getMpd() function
@params- mpd list
@returns- 
'''
def countMpdCallGetUrl(mpdList):
	
	count=len(mpdList)
	for index,url in enumerate(mpdList):
		#print url
		#call to getMpd()
		xmlName=getMpd(url,index)	
		#call parseMpd()
		parseMpd(xmlName)
		os.chdir("..")


'''
Function to create a directory
@params-index- the index of the list will be used to create separate directories
'''
def createDir(index):
	path = "./"+str(index)

	if not os.path.exists(path):
    		os.makedirs(path)
	return str(path)

'''
function to fetch the url of the mpd incase the base url is not an http://
@returns the url of the mpd file after truncating the end which will serve as absolute url of the video files.
'''
def returnMpdUrl():
	urlPath="./url.txt"
	fileUrl = open(urlPath, "r")
	absUrl=str(fileUrl.readline())
	#print absUrl
	#print "After Splitting:"
	#print (re.split("/",absUrl))
	absSplit=re.split("/",absUrl)
	absSplit.pop()
	#print absSplit
	s="/".join(absSplit)
	
	#print s+'/'
	return s+'/'


'''
function to download
@params completeUrl,fName
'''
def download(completeURL,fname):
	testfile = urllib.URLopener()
	testfile.retrieve(completeURL,fname)
	return

'''
function to write all mpd files to individual xml files
1)calls createDir() which creates a dir with the index no as dir name
@params: url of individual mpd,index number(also the name of the directory)
calls checkIfBaseUrlExist()
'''
def getMpd(url,index):
  	file_name = url.replace('https://', '').replace('.', '_').replace('/','_')
	print url
	response = urllib2.urlopen(url)
	
	path=createDir(index)
	os.chdir(path)
	fileUrl = open("url.txt", "w")
	fileUrl.write(url)
	xmlName=str(index)+".xml"
     	with open(xmlName, 'w') as out_file:
		shutil.copyfileobj(response, out_file)
	fileUrl.close
	return xmlName



'''
function to check if absolute base url exist,i.e. if base url is an http://
@params-xml/mpd file
@returns-the absolute url if it is http://
else returns the url of the mpd file as absolute url
'''

def extractAbsoluteBaseUrl(xmlName):
	doc = minidom.parse(xmlName)
	AbsoluteUrl=doc.getElementsByTagName("BaseURL")[0]
	print (AbsoluteUrl.firstChild.data)
	#match the AbsoluteUrl to check if it has http://
	if re.match('^http://', AbsoluteUrl.firstChild.data):
		#return the AbsoluteUrl		
		return AbsoluteUrl.firstChild.data
	else:
		#return the url of the mpd file
		absUrl=returnMpdUrl()
		return absUrl	


'''
function to check mimeType in Representation tag for "video/mp4" and extract all relative urls for the video files
1)calls download()
@params-xml/mpd file name
@bUrl-Absolute url of the video/mp4
'''
def computeCompleteUrl(xmlName,bUrl):
	doc = minidom.parse(xmlName)
	Representations=doc.getElementsByTagName("Representation")
	for Representation in Representations:
		mimeType=Representation.getAttribute("mimeType")
		if mimeType=="video/mp4":
			
			BaseURLs=Representation.getElementsByTagName("BaseURL")
			for BaseURL in BaseURLs:
				url=BaseURL.firstChild.data
				completeUrl=bUrl+url
				return completeUrl
				#call the download function here to download the video
				download(completeUrl,url)
				#print (completeUrl)

'''
function for segmented part
'''


'''
function to calculate duration in seconds from a split list of time intervals
@params -duration-split list of time 
'''
def calcDuration(duration):
	
	length=len(duration)
	#print length
	if length==4:
		hour=duration[1]
		hour=hour[:-1]
		#print hour
		mins=duration[2]
		mins=mins[:-1]
		#print mins
		sec=duration[3]
		sec=sec[:-1]
		#print sec
		hour=int(hour)*3600
		mins=int(mins)*60
		sec=float(sec)
		totalDuration=math.ceil(float(hour+mins+sec))
		return totalDuration
	elif length==3:
		mins=duration[1]
		mins=mins[:-1]
		#print mins
		sec=duration[2]
		sec=sec[:-1]
		#print sec
		
		mins=int(mins)*60
		sec=float(sec)
		totalDuration=math.ceil(float(mins+sec))
		return totalDuration
	elif length==2:
		sec=duration[1]
		sec=sec[:-1]
		#print sec
		totalDuration=math.ceil(float(sec))
		return totalDuration
		
	



'''
function to parse xml/mpd files that has no base url defined
checks is maxSegmentDuration is present expilicitly or calls the calcSegLen() to calculate the same
@params-xmlName-name of xml/mpd file
@params-totalDuration-the total duration

calculates total no of segments as: totalDuration/maxSegmentDuration
'''

def details(xmlName,totalDuration):
		
		doc = minidom.parse(xmlName)
		AdaptationSets=doc.getElementsByTagName("AdaptationSet")
		for AdaptationSet in AdaptationSets:
			
			if AdaptationSet.hasAttribute("mimeType"):
				mimeType=AdaptationSet.getAttribute("mimeType")

				if mimeType=="video/mp4":
					
					SegmentTemplates=AdaptationSet.getElementsByTagName("SegmentTemplate")
					for SegmentTemplate in SegmentTemplates:
						
						timescale=SegmentTemplate.getAttribute("timescale")
						if SegmentTemplate.hasAttribute("duration"):
							duration=SegmentTemplate.getAttribute("duration")
							#calculate no of segments
							maxSegDur=float(duration)/float(timescale)
							maxSegmentDuration=int(math.ceil(maxSegDur))
							noOfSeg=int(math.ceil(totalDuration/maxSegmentDuration))
							print int(noOfSeg)
							#get media name for that segment
							media=SegmentTemplate.getAttribute("media")
							#print media
							#get init seg name
							#get init seg name
							intSeg=SegmentTemplate.getAttribute("initialization")
							
							command=[]
							commandGen=''
							command.append("cat")
							


							for index in range(0,noOfSeg):
								#generate mp4 url and download it
								url=segVideoMp4Url(media,index+1)
								command.append(url)
								
								
							command.append(">")
							command.append(intSeg)
							commandGen=" ".join(command)
							#callfunction to execute concatenate command
							p=subprocess.Popen(commandGen,shell=True)
							#call generate csv function streamID=url(i.e filename)
							generateCsv(url,intSeg,intSeg)
							delMp4(intSeg)
			
							
						else:	
							sTags=SegmentTemplate.getElementsByTagName("S")
							for S in sTags:
								duration=int(S.getAttribute("d"))
								#calculate no of segments
								maxSegDur=float(duration)/float(timescale)
								maxSegmentDuration=int(math.ceil(maxSegDur))
								noOfSeg=int(math.ceil(totalDuration/maxSegmentDuration))
								print int(noOfSeg)
								#get media name for that segment
								media=SegmentTemplate.getAttribute("media")
								#print media
								#get init seg name
								intSeg=SegmentTemplate.getAttribute("initialization")
								
								command=[]
								commandGen=''
								command.append("cat")
								

								for index in range(0,noOfSeg):
									#generate mp4 url and download it
									url=segVideoMp4Url(media,index+1)
									command.append(url)
									
								command.append(">")
								command.append(intSeg)
								commandGen=" ".join(command)
								#callfunction to execute concatenate command
								p=subprocess.Popen(commandGen,shell=True)
								p.wait()
								#call generate csv function streamID=url(i.e filename)
								generateCsv(url,intSeg,intSeg)
								delMp4(intSeg)
			
				
					
			
			else:
				
				Representations=AdaptationSet.getElementsByTagName("Representation")
				for Representation in Representations:
					if Representation.hasAttribute("mimeType"):
						mimeType=Representation.getAttribute("mimeType")
					
						if mimeType=="video/mp4":
							
							SegmentTemplates=Representation.getElementsByTagName("SegmentTemplate")
							for SegmentTemplate in SegmentTemplates:
								timescale=SegmentTemplate.getAttribute("timescale")
								if SegmentTemplate.hasAttribute("duration"):
									duration=int(SegmentTemplate.getAttribute("duration"))
									#calculate no of segments
									maxSegDur=float(duration)/float(timescale)
									maxSegmentDuration=int(math.ceil(maxSegDur))
									noOfSeg=int(math.ceil(totalDuration/maxSegmentDuration))
									print int(noOfSeg)
									#get media name for that segment
									media=SegmentTemplate.getAttribute("media")
									#print media
									#get init seg name
									intSeg=SegmentTemplate.getAttribute("initialization")
									 
									command=[]
									commandGen=''
									command.append("cat")
									

									for index in range(0,noOfSeg):
										#generate mp4 url and download it
										url=segVideoMp4Url(media,index+1)
										command.append(url)
									command.append(">")
									command.append(intSeg)
									#print command
									commandGen=" ".join(command)
									#callfunction to execute concatenate command
									p=subprocess.Popen(commandGen,shell=True)
									#call generate csv function streamID=url(i.e filename)
									generateCsv(url,intSeg,intSeg)
									delMp4(intSeg)
				
						
								else:
									sTags=SegmentTemplate.getElementsByTagName("S")
									for S in sTags:
										duration=int(S.getAttribute("d"))
										#calculate no of segments
										maxSegDur=float(duration)/float(timescale)
										maxSegmentDuration=int(math.ceil(maxSegDur))
										noOfSeg=int(math.ceil(totalDuration/maxSegmentDuration))
										print int(noOfSeg)
										#get media name for that segment
										media=SegmentTemplate.getAttribute("media")
										#print media
										#get init seg name
										intSeg=SegmentTemplate.getAttribute("initialization")
										
										command=[]
										commandGen=''
										command.append("cat")
									

										for index in range(0,noOfSeg):
											#generate mp4 url and download it
											url=segVideoMp4Url(media,index+1)
											command.append(url)
										
										command.append(">")
										command.append(intSeg)
										commandGen=" ".join(command)
										#callfunction to execute concatenate command
										p=subprocess.Popen(commandGen,shell=True)
										#call generate csv function streamID=url(i.e filename)
										generateCsv(url,intSeg,intSeg)
										delMp4(intSeg)
			


'''
function to generate absolute mp4 url for segmented videos
@params-mediaName- the name of the file
@index-the index no will be placed in the $number/time$ place in order to get the complete valid url
@returns name of the mp4 segment
'''
def segVideoMp4Url(mediaName,index):
	
	print mediaName
	#get the url of the mpd
	mpdUrl=returnMpdUrl()

	#replace the $Number$/$Time$ with seq no
	url=re.sub("\$\w*\$",str(index),mediaName)

	#also write the video name to file 
	fileUrl = open("video_List.txt", "a")
	fileUrl.write(url)


	completeUrl=mpdUrl+url
	
	print completeUrl
	'''
	fileUrl = open("url_list.txt", "a")
	fileUrl.write(completeUrl)
	'''
	download(completeUrl,url)
	return url
		
#segVideoMp4Url()

'''
function to check if the mpd file has its total duration mentioned explicitly or if required to collect from period tag
It will accordingly calculate the totalDuration of each period and call the details() which will get the required details
@params-xmlName-the xml/mpd file name to be supplied
'''
def parseNonBaseUrlMpd(xmlName):

	doc = minidom.parse(xmlName)
	mpd=doc.getElementsByTagName("MPD")
	for MPD in mpd:
		
		mediaPresentationDuration=MPD.getAttribute("mediaPresentationDuration")
		Duration=re.findall('PT|\d*\H|\d*\M|\d*\.?\d*\S', mediaPresentationDuration)
		print Duration
		totalDuration=calcDuration(Duration)
		print totalDuration
		Periods=doc.getElementsByTagName("Period")		
		for Period in Periods:
			if Period.hasAttribute("duration"):
				duration=Period.getAttribute("duration")
				Duration=re.findall('PT\d*\H\d*\M\d*\.?\d*\S', duration)
				#print Duration
				totalPeriodDuration=calcDuration(Duration)
				if totalPeriodDuration>0:
					details(xmlName,totalPeriodDuration)
				else:	
					details(xmlName,totalDuration)
			else:
				
				details(xmlName,totalDuration)




'''
segmented ends here
'''

'''
function to check if the mpd file has an absolute url to mp4 files
1)calls extractAbsoluteBaseUrl()-returns the absolute base url
2)calls the computeCompleteUrl()-returns the complete video url
@params-xmlname- xml/mpd file to parse
'''
def checkIfBaseUrlExist(xmlName):
	doc = minidom.parse(xmlName)
	BaseURL=doc.getElementsByTagName("BaseURL")
	if (BaseURL.length>0):
		
		#call extractAbsoluteBaseUrl if the base url has http://
		bUrl=extractAbsoluteBaseUrl(xmlName)
		#call computeCompleteUrl once the absolute base url is retrived and pass to computeCompleteUrl()
		computeCompleteUrl(xmlName,bUrl)
		#call generateCsv()
		generateCsv(bUrl,bUrl,bUrl)
		delMp4(bUrl)
	else:
		print "No Base url"
		
		#case for segmented video
		parseNonBaseUrlMpd(xmlName)




def generateCsv(streamId,fName,videoName):
	'''
	streamID='ED_512_640K_MPEG2_video_init'
	fName='ED_512_640K_MPEG2_video_init.csv'
	videoName='ED_512_640K_MPEG2_video_init.mp4'
	'''
	cmd='./ffmpeg -qubitOpts "QubitInfo=ver1;stream-Id=$1;sessionType=vod;fileName=$2;" -i $3 -f null null.out'
	cmd=re.sub("\$1",streamId,cmd)
	cmd=re.sub("\$2",fName,cmd)
	cmd=re.sub("\$3",videoName,cmd)
	p=subprocess.Popen(cmd,shell=True)
	p.wait()

'''
function to delete mp4 file
'''
def delMp4(fName):
	os.remove(fName)


'''
function to parse each mpd file 
1)calls checkIfBaseUrlExist()
'''
def parseMpd(xmlName):
	doc = minidom.parse(xmlName)
	mpd=doc.getElementsByTagName("MPD")
	for MPD in mpd:
		mediaType=MPD.getAttribute("type")
		if mediaType=="static":
			checkIfBaseUrlExist(xmlName)

#call to generateMpdList()
mpdList=generateMpdList("mpd.txt")
#main 
countMpdCallGetUrl(mpdList)

